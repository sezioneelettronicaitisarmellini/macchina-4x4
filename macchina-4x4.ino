#include "Ultrasonic.hpp"

constexpr uint16_t delaySensor = 100; //delay between sensors reading in milliseconds
constexpr uint16_t delayCommand = 250; //stop the car if the car does not receive a command in this interval

constexpr uint8_t speedRight = 3,
                  speedLeft = 11,
                  dirRight = 12,
                  dirLeft = 13,
                  brakeRight = 9,
                  brakeLeft = 8,
                  sensorBTrigger = 2,
                  sensorBEcho = 5,
                  sensorFTrigger = 6,
                  sensorFEcho = 10,
                  piezo = 7;


Ultrasonic frontSensor(sensorFTrigger, sensorFEcho);
Ultrasonic backSensor(sensorBTrigger, sensorBEcho);


String serialString;


int16_t speedValue = 0,
        dirValue = 0,
        speedLeftValue = 0,
        speedRightValue = 0,
        dirLeftValue = 0,
        dirRightValue = 0,
        frontDistance = 0,
        backDistance = 0,
        backDistanceSet = 0,
        frontDistanceSet = 0,
        absSpeedValue = 0;


bool emergencyState = false,
     emergencyFront = false,
     emergencyBack = false,
     forwardRight = false,
     forwardLeft = false,
     brakeLeftValue = false,
     brakeRightValue = false,
     autoMode = false,
     lock = false;

uint32_t lastTimeSensor = millis();
uint32_t lastTimeCommand = millis();


void readSpeed() { // this parse the string to get velocity
  speedValue = serialString.substring(serialString.indexOf('Y') + 1).toInt();
  if (speedValue < -50) { // a dead zone
    absSpeedValue = map(abs(speedValue), 0, 100, 0, 255); //maps the speed from -100 and +100 to 0-255 for the 8bit pwm
    forwardRight = forwardLeft = true;
  }
  else if (speedValue > 50) {
    absSpeedValue = map(abs(speedValue), 0, 100, 0, 255);
    forwardRight = forwardLeft = false;
  }
  else {
    absSpeedValue = 0;
  }
}


void readDirection() { //this function parse the the string to get the direction
  dirValue = serialString.substring(1, serialString.indexOf('Y')).toInt();
  if (dirValue < -35) { //this is a dead zone to prevent involuntary movements
    dirRightValue = 100;
    dirLeftValue = 20;
    forwardLeft = !forwardLeft;
  }
  else if (dirValue > 35) {
    dirLeftValue = 100;
    dirRightValue = 20;
    forwardRight = !forwardRight;
  }
  else {
    dirLeftValue = 100;
    dirRightValue = 100;
  }
}


void setMotors() { // check if it is safe to move and set the motors speed
  if (!(emergencyState || emergencyFront || emergencyBack ) || lock) { // if there are not obstacles and the car is not in the emergency state, or if the readings are ignored, sets the speed of motors
    speedLeftValue = absSpeedValue * (dirLeftValue / 100);
    speedRightValue = absSpeedValue * (dirRightValue / 100);
    if (brakeLeftValue) {
      brakeLeftValue = brakeRightValue = false;
    }
  }
  else { //else it sets the brakes
    speedLeftValue = 0;
    speedRightValue = 0;
    brakeLeftValue = true, brakeRightValue = true;
  }
  digitalWrite(dirRight, forwardRight);
  digitalWrite(dirLeft, forwardLeft);
  // Sets the motors speed
  analogWrite(speedLeft, (int)speedLeftValue);
  analogWrite(speedRight, (int)speedRightValue);
  // Sets the brake
  digitalWrite(brakeLeft, brakeLeftValue);
  digitalWrite(brakeRight, brakeRightValue);
}


void sensor() { // this function read the sensors and sets some flags if the distances are smaller than the safe distances
  frontDistance = frontSensor.read();
  backDistance = backSensor.read();
  Serial.println("*D" + String(frontDistance) + "\n*"); //print the readings for debug purposes
  Serial.println("*D" + String(backDistance) + "\n*");
  emergencyFront = (frontDistance <= frontDistanceSet && frontDistance != 0 && speedValue < 0 );
  emergencyBack = (backDistance <= backDistanceSet && backDistance != 0 && speedValue > 0 );
}


void setup() {
  uint8_t pins[] = {speedLeft, speedRight, dirLeft, dirRight, brakeLeft, brakeRight }; //Initialization of pins
  for (uint8_t i = 0; i < sizeof(pins) / sizeof(pins[0]); i++)
    pinMode(pins[i], OUTPUT);
  Serial.begin(9600); //Initialization of serial transmission
  lastTimeSensor = millis();
  lastTimeCommand = millis();
}


void loop() {
  uint32_t timeNow = millis(); // get the current time in milliseconds
  if (timeNow - lastTimeSensor > delaySensor ) { //Read the sensors every delaySensor 
    lastTimeSensor = timeNow;
    sensor();
    setMotors();
  }
  if (timeNow - lastTimeCommand > delayCommand ) { //if the car does not receive a command for delayCommand time stops the car
    absSpeedValue = 0;
    setMotors();
  }
  if (Serial.available() > 0) {
    serialString = Serial.readStringUntil(';'); //read the serial buffer until ';' character
    switch (serialString[0]) { //Check the first character of the string
      case 'X':
        lastTimeCommand = millis();
        readSpeed();
        readDirection();
        sensor();
        break;
      case 'L': //if the character is 'L' the car will ignore the readings of the sensors until a new 'L' is received
        lock = !lock;
        if (lock) {
          Serial.println("*JR255G0B0*"); //Sets a light on the app as warning if the readings are ignored
        }
        else {
          Serial.println("*JR0G0B0*"); //turn off the light if the readings are checked
        }
        break;
      case 'A':
        frontDistanceSet = serialString.substring(1).toInt(); // sets the distance to stop if an obstacle is encountered
        break;
      case 'E': // toggle the emergency state no command are recived in this state
        emergencyState = !emergencyState;
        if (emergencyState)
          Serial.println("*HR255G0B0*");
        else
          Serial.println("*HR0G255B0*");
        break;
      case 'B':
        backDistanceSet = serialString.substring(1).toInt(); // sets the distance for obstacles in the back
        break;
      case 'Y': // makes a buzzer ring
        if (serialString.substring(1).toInt())
          noTone(piezo);
        else
          tone(piezo, 440);
        break;
      default:
        break;
    }
    setMotors(); // sends an update to the motors
  }
}
